﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeGame
{
    /// <summary>
    /// 存储地图信息
    /// </summary>
    public class Map
    {
        public MapUnit[][] units { get; set; }//数组
        public List<Group> groups { get; set; }//组集合
        public bool isInited { get; set; }//地图是否初始化了
       /// <summary>
       /// 构造map对象 
       /// </summary>
        public Map()
        {
            //数组初始化
            units=new MapUnit[10][];
            for (int i = 0; i < 10; i++) {
                units[i] = new MapUnit[10];
            }
            groups = new List<Group>();//分组初始化
        }
        /// <summary>
        /// 通过字符串初始化
        /// </summary>
        /// <param name="str"></param>
        public void initMap(String str) {
           
            String[] lines = str.Split('|');//竖线分开
            for (int i = 0; i < 10; i++)//循环行
            {
                if (lines[i].Length == 0) continue;
                string[] cols = lines[i].Split(':');
                for (int j = 0; j < 10; j++)//循环列
                {
                    if (cols[j].Length == 0) continue;
                    int v = int.Parse(cols[j]);//转换为整数
                    MapUnit unit = new MapUnit(v);//创建一个单元
                    units[i][j] = unit;//存放到数组
                    unit.x = i;//记录行列
                    unit.y = j;

                }
            }
            //初始化完毕，使用递归的方式将单元分组
            int no = 1;//分组从1开始
            groups.Clear();//清空原来的分组信息
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (units[i][j].Type == MapUnit.LAND && units[i][j].GroupNo == 0)
                    {
                        Group g = new Group(no);
                        groups.Add(g);
                        doGroup(i, j, g);
                        no++;//编号增加1
                    }
                }
            }
            isInited = true;//初始化完毕
        }
        /// <summary>
        /// 递归分组方法
        /// </summary>
        /// <param name="i">行标</param>
        /// <param name="j">列标</param>
        /// <param name="g">分组</param>
        void doGroup(int i, int j, Group g) {
            if (i >= 10 || j >= 10 || units[i][j].GroupNo!=0 || units[i][j].Type!=MapUnit.LAND) return;//超出范围直接忽略
            g.Add(units[i][j]);//存放进入组

            if (!units[i][j].BorderRight ) { 
                //右侧不是边界
                doGroup(i , j+ 1, g);
            }
            if (!units[i][j].BorderBottom) { 
                //下面不是边界
                doGroup(i+ 1, j , g);
            }
            if (!units[i][j].BorderTop)
            {
                //上面不是边界
                doGroup(i - 1, j, g);
            }
            if (!units[i][j].BorderLeft)
            {
                //左边
                doGroup(i , j-1, g);
            }

        }

        
    }

}
