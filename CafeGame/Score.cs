﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CafeGame
{
    public class Score
    {
        public int User { get; set; }
        public int Computer { get; set; }
        public int x { get; set; }
        public int y { get; set; }
    }
}
