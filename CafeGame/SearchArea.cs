﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CafeGame
{
    class area {
        List<MapUnit> units = new List<MapUnit>();//存储单元集合
        public int Score { get { return units.Count; } }
        public void Add(MapUnit u) {

            units.Add(u);
        }
    }
    /// <summary>
    /// 寻找最大连通区域
    /// </summary>
    public class SearchArea
    {
        bool[,] flag;//是否遍历过的伴随矩阵
        List<area> areas;//区域集合
        public Map map { get; set; }
        private int owner;
        /// <summary>
        /// 返回某个拥有者的最大面积得分
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public int GetScore(int owner) {
            this.owner = owner;
            areas = new List<area>();//区域集合
           flag=new bool[10,10];//初始化，全是false

           for (int y = 0; y < 10; y++)
           {
               for (int x = 0; x < 10; x++)
               {
                   
                    search(x, y);//递归遍历
                  
                   
               }
           }
            int max=0;
            foreach (area a in areas) {
                if (a.Score > max) max = a.Score;
            
            }

           return max;
        }
        //递归
        private void search(int x, int y,area ar=null) {
            if (x <= 0 || y <= 0 || x >= 9 || y >= 9) return;//超过就不管了
            if (flag[y, x]) return;//已经遍历过了
            flag[y, x] = true;//标记
            if (map.units[y][x].Owner == owner)
            {
                if (ar == null)
                {
                    ar = new area();
                    areas.Add(ar);//添加到区域集合
                }
                ar.Add(map.units[y][x]);//添加至

                search(x, y - 1, ar);//上
                search(x+1, y , ar);//又
                search(x, y + 1, ar);//下
                search(x-1, y , ar);//左

            }
           
        }

    }

}
