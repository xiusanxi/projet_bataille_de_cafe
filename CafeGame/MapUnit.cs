﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeGame
{
    /// <summary>
    /// 
    /// 表示一个地图上的小格子
    /// </summary>
    public class MapUnit
    {
        public static  int FOREST=32;//深林
        public static  int SEA = 64;//海域
        public static  int LAND = 0;//耕地

        public static  int OWNERNO = 0;//空白
        public static  int OWNERUSER = 1;//玩家
        public static  int OWNERCOMPUTER = 2;//电脑

        public bool BorderTop { get; set; }//上边界
        public bool BorderRight { get; set; }//右边界
        public bool BorderBottom { get; set; }//下边界
        public bool BorderLeft { get; set; }//左边界
        public int GroupNo { get; set; }//组编号,1,2,3。。。相同编号的表示是同一个组，同一块地里的
        public int Type { get; set; }//0，1，2
        public int Owner { get;set;}//归谁所有0：空地 1 玩家 2电脑
        public int x { get; set; }//所属的列
        public int y { get; set; }//所属行

        /// <summary>
        /// 使用一个整数来初始化
        /// </summary>
        /// <param name="v"></param>
        public MapUnit(int v) {
            byte b = (byte)v;
            BorderTop = (b & 0x01) != 0;
            BorderLeft = (b & 0x02) != 0;
            BorderBottom = (b & 0x04) != 0;
            BorderRight = (b & 0x08) != 0;
            
            Type = v & 0xf0;//类型
            Owner = OWNERNO;//没有人占用
            GroupNo = 0;//没有分组
            
            
        }
    }
}
