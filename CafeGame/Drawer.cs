﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
namespace CafeGame
{
    /// <summary>
    /// 完成游戏地图的绘制
    /// </summary>
    public class Drawer
    {
        private Bitmap bmp;//绘图的图片
        public Map map = new Map();//存储数据对象

        private PictureBox picbox;//用于显示图片的控件

        private int UnitWidth;//单元的宽度
        private int UnitHeight;//单元高度
        /// <summary>
        /// 传入显示图片的控件进行初始化
        /// </summary>
        /// <param name="p">控件</param>
        public Drawer(PictureBox p)
        {
            this.picbox = p;
            bmp = new Bitmap(p.Width, p.Height);//新建一样的图片

            UnitWidth = (p.Width - 11 )/ 10;//宽度为全部宽度去掉11个像素，平分为10个
            UnitHeight = (p.Height - 11) / 10;//单元格高度

            
        }
        /// <summary>
        /// 把鼠标的坐标转换为行和列
        /// </summary>
        public String GetPosition(int x,int y) {
            x = x / UnitWidth;//第几Lie
            y = y / UnitHeight;//第几个hang
            if (map.units[y][x]==null || map.units[y][x].Type != MapUnit.LAND || map.units[y][x].Owner != 0) return null;//不能点的
            return String.Format("A:{0}{1}",y,x);

        }


        
        /// <summary>
        /// 有鼠标移动的绘制方法，鼠标移动到LAND上，土地变色
        /// 前提是空的土地
        /// </summary>
        public void Draw(int x=-1,int y=-1)
        {
            
            x = x / UnitHeight;//第几行
            y = y / UnitWidth;//第几个单元格
            Graphics g = Graphics.FromImage(bmp);//获取图像的画布
            g.Clear(Color.White);//背景白色
            Pen pline=new Pen(Color.Black);


            Brush SeaBrush=new SolidBrush(Color.Blue);//海洋画刷
            Brush ForestBrush = new SolidBrush(Color.ForestGreen);//深林画刷
            Brush LandBrush = new SolidBrush(Color.Gold);//陆地画刷
            Brush LandBrush1 = new SolidBrush(Color.YellowGreen);//陆地画刷1
            Brush uBrush = new SolidBrush(Color.Green);//用户的土地画刷
            Brush cBrush = new SolidBrush(Color.WhiteSmoke);//电脑的土地画刷
            Brush FontBrush=new SolidBrush(Color.Black);//文字
            Font font=new Font("黑体",12);//文字

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {

                    MapUnit u=map.units[i][j];//单元格
                    if (u == null) return;//为空直接退出
                    int sx =  j + j * UnitWidth;//左上角开始的坐标
                    int sy = i + i* UnitHeight;//左上开始的坐标
                    
                    if (u.Type == MapUnit.FOREST) {//绘制森林
                        g.FillRectangle(ForestBrush, sx, sy, UnitWidth+2, UnitWidth+2);
                    }
                    else if (u.Type == MapUnit.SEA)
                    {
                        //绘制海洋
                        g.FillRectangle(SeaBrush, sx, sy, UnitWidth+2, UnitWidth+2);
                    }
                    else {
                        //绘制陆地
                        if ( u.Owner==0 &&  x == i && y == j )//当前鼠标移上去的
                            g.FillRectangle(LandBrush, sx, sy, UnitWidth + 2, UnitWidth + 2);
                        else
                        {
                            Brush b = LandBrush1;
                            if (u.Owner == MapUnit.OWNERUSER) b = uBrush;//用户的土地
                            else if (u.Owner == MapUnit.OWNERCOMPUTER) b = cBrush;//电脑的土地

                            g.FillRectangle(b, sx, sy, UnitWidth + 2, UnitWidth + 2);
                        }

                        if (u.BorderTop) g.DrawLine(pline, sx, sy, sx + UnitWidth + 2, sy);//上
                        if (u.BorderRight) g.DrawLine(pline, sx + UnitWidth + 2, sy, sx + UnitWidth + 2, sy + UnitHeight + 2);//右
                        if (u.BorderBottom) g.DrawLine(pline, sx + UnitWidth + 2, sy + UnitHeight + 2, sx, sy + UnitHeight + 2);//下
                        if (u.BorderLeft) g.DrawLine(pline, sx, sy + UnitHeight + 2, sx, sy);//左

                        char t = (char)('a' + u.GroupNo-1);
                        string txt = "" + t;
                        g.DrawString(txt, font, FontBrush, sx + UnitWidth / 2 - 3, sy + UnitHeight / 2 - 6);
                    }
                   
                }
            }




            Refresh();

        }
        /// <summary>
        /// 显示图片
        /// </summary>
        public void Refresh() {
            picbox.Image = bmp;//显示
        }
        /// <summary>
        /// 使用地图字符串初始化地图
        /// </summary>
        /// <param name="str"></param>
        public void initMap(String str) {
            //67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|
            map.initMap(str);
        }

    }
}
