﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
namespace CafeGame
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private SearchArea sa;
        Drawer drawer;
        private NetClient client = new NetClient();
        int seeds = 28;//种子数量
        int step = 0;//0==绘制地图，1==电脑发送指令
        String cmd;//名称
        void onReviceMsg(String msg) {
            if (step == 0)
            {
                drawer.initMap(msg);//初始化
                drawer.Draw();
                step = 1;
                lblinfo.Text = "游戏开始";
                Thread.Sleep(1000);
                doFirstStep();
                //client.Send("Start");
            }
            else {
                if (msg.IndexOf("VALI")!=-1) {
                    DoSeed(cmd);//用户
                    step = 2;//切换为电脑走

                }
                if (msg.IndexOf("B:") != -1) { 
                    //电脑走的结果
                    DoSeed(msg.Substring(msg.IndexOf("B:")));//电脑
                    step = 1;


                }
                if (msg.IndexOf("INVA")!=-1)
                {
                   //不合法的走位
                    MessageBox.Show("不合法");
                }
                if (msg.IndexOf("FINI") != -1)
                {
                    step = 4;
                    MessageBox.Show("Game Over");
                }
                if(msg.IndexOf("S")!=-1){
                    //评分S:45:40
                    String[] arr = msg.Substring(msg.IndexOf("S")).Split(':');//分割
                    lblinfo.Text = "游戏结束,玩家得分:" + arr[1] + ",电脑得分:" + arr[2];
                
                }


                MessageBox.Show(msg);
            }
        }
        private void btnConnect_Click(object sender, EventArgs e)
        {
            client.reciveMessage += this.onReviceMsg;
            client.DoConnect(txtIP.Text, int.Parse(txtPort.Text));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form.CheckForIllegalCrossThreadCalls = false;
            pic.Width = 38 * 10 + 11;
            pic.Height = 38 * 10 + 11;
            drawer = new Drawer(pic);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            client.DIsConnect();
        }

        private void pic_MouseMove(object sender, MouseEventArgs e)
        {
            //不能自己走了
            return;
            //if (step !=1) return;//不是1，不能开始
            drawer.Draw(e.Y, e.X);//绘制当前的鼠标经过效果
        }

       

        private void pic_MouseDown(object sender, MouseEventArgs e)
        {
            return;
            cmd = drawer.GetPosition(e.X, e.Y);
            if (cmd != null) {
                client.Send(cmd);

            }
        }
        /// <summary>
        /// 标记地块所有者
        /// </summary>
        /// <param name="cmd"></param>
        void DoSeed(string cmd)
        {
            seeds--;
            String u = cmd.Substring(0, 1);
            int y = int.Parse(cmd.Substring(2, 1));
            int x= int.Parse(cmd.Substring(3, 1));
            if (u.Equals("A"))
            {

                drawer.map.units[y][x].Owner = MapUnit.OWNERUSER;
                drawer.Draw();//重新绘制
            }
            else
            {
                drawer.map.units[y][x].Owner = MapUnit.OWNERCOMPUTER;

                drawer.Draw();//重新绘制
                Auto(x, y);

            }
           
            
        }

        /// <summary>
        /// 计算总分数,返回分数对象
        /// </summary>
        private Score CalScore()
        {
            Score s = new Score();
            foreach (Group g in drawer.map.groups) {
                if (g.Winner == MapUnit.OWNERUSER) {
                    s.User += g.units.Count;//玩家得分

                
                }
                else if (g.Winner == MapUnit.OWNERCOMPUTER) {
                    s.Computer += g.units.Count;//电脑得分
                
                }
            
            }


            if (sa == null) {
                sa = new SearchArea();
                sa.map = drawer.map;
            }
            int u = sa.GetScore(MapUnit.OWNERUSER);//用户最大连通得分
            int c = sa.GetScore(MapUnit.OWNERCOMPUTER);//电脑最大连通得分
            s.User += u;
            s.Computer += c;
            return s;
        }
        //第一步
        private void doFirstStep()
        { 
            
            //第一步
            Group g = null;

            foreach (Group i in drawer.map.groups) {
                if (g == null || g.units.Count < i.units.Count) g = i;
            
            }
            if (g == null)
            {
                cmd = "A:11";
            }
            else {
                cmd = String.Format("A:{0}{1}", g.units[0].x, g.units[0].y);
            
            }
            client.Send(cmd);//发送
        }
        /// <summary>
        /// 自动走
        /// </summary>
        private void Auto(int x,int y)
        {

            List<MapUnit> cans = new List<MapUnit>();
            //寻找可以走的块

            for (int i = 1; i < 9; i++) {
                //垂直
                if (Math.Abs(x - i) < 2) continue;//距离不够
                if (drawer.map.units[y][i].Owner == MapUnit.OWNERNO) {
                    cans.Add(drawer.map.units[y][i]);
                }
            }

            for (int i = 1; i < 9; i++)
            {
                //水平
                if (Math.Abs(y - i) < 2) continue;//距离不够
                if (drawer.map.units[i][x].Owner == MapUnit.OWNERNO)
                {
                    cans.Add(drawer.map.units[i][x]);
                }
            }
            // 循环计算分数
            List<Score> scores = new List<Score>();//分数集合
            foreach (MapUnit unit in cans) {
                unit.Owner = MapUnit.OWNERUSER;//临时占用
                Score s = CalScore();//计算得分
                s.x = unit.x;
                s.y = unit.y;//记录坐标
                scores.Add(s);
                unit.Owner = MapUnit.OWNERNO;//解除暂用
            
            }

            //寻找最大分数
            List<Score> max = new List<Score>();
            int maxscore = 0;//存最高分
            foreach (Score sc in scores) {
                if (sc.User > maxscore) maxscore = sc.User;
            
            }
            //把最高分的走法拿出来
            foreach (Score sc in scores)
            {
                if (sc.User == maxscore) {
                    max.Add(sc);
                }

            }
            //寻找最低分
            Score min = max[0];//假设最低
            foreach (Score s in max) {

                if (s.Computer < min.Computer) {
                    min = s;
                }
            }
            Thread.Sleep(1000);
            cmd = String.Format("A:{0}{1}", min.x, min.y);
           // MessageBox.Show(cmd);
            client.Send(cmd);

        }
    }
}
