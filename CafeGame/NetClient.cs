﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Windows.Forms;
namespace CafeGame
{
    public delegate void onReciveMessage(String msg); //定义接受信息的代理类型

    public class NetClient
    {
        public string Ip{ get; set; }//目标ip地址
        public int Port{ get; set; }//目标端口
        public onReciveMessage reciveMessage { get; set; }

      
        private Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);//socket对象，用来链接服务器和收发数据
        private bool isRun = false;//是否在运行
        private Thread trd;//用来接受数据的线程，在线程里面接受数据
        private byte[] buffer = new byte[2048];//定义2k的缓存区

        public bool Connected {
            get { return socket.Connected; }//只读属性
        }

        public void DIsConnect() {
            isRun = false;
           
        }
        
        public void DoConnect(String ip1,int port)//链接到服务器
        {
            this.Ip = ip1;
            this.Port = port;
            IPAddress ip = IPAddress.Parse(Ip);//字符串转换位ip地址对象
            IPEndPoint endpoint = new IPEndPoint(ip, Port);//链接目标对象
            
            socket.Connect(endpoint);//链接
           
            isRun = true;
            if (trd == null) trd = new Thread(messageloop);
            trd.Start();


        }

        /// <summary>
        /// 发送命令
        /// </summary>
        /// <param name="cmd"></param>
        public void Send(String cmd)
        {
            if (socket.Connected)
            {
                byte[] byteArray = System.Text.Encoding.ASCII.GetBytes(cmd);
                int a = socket.Send(byteArray);
            }
            else {
                MessageBox.Show("链接断开");
            }
        }
        /// <summary>
        /// 
        /// 接受数据的线程方法
        /// </summary>
        private void messageloop()
        {
            while (isRun ) {
                if (socket.Connected)
                {
                    Thread.Sleep(10);
                    try
                    {
                        //如果在运行，并且是链接成功的
                        int len = socket.Receive(buffer);
                        if (len > 0)//如果有数据
                        {
                            String msg = Encoding.UTF8.GetString(buffer, 0, len);
                            if (reciveMessage != null) reciveMessage(msg);//触发事件
                        }
                    }
                    catch (Exception ex) {
                       // MessageBox.Show("连接断开");
                    
                    }
                }
                //Thread.Sleep(50);//休息50毫秒
            
            }
           // socket.Disconnect(true);//关闭链接
           // connected = false;//改变状态
            trd.Abort();//停止线程
        }
       



    }
}
