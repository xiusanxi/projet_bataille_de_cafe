﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CafeGame
{
    /// <summary>
    /// 
    /// 将不同的块分入不同的组，计分使用
    /// </summary>
    public class Group
    {
        public List<MapUnit> units { get; set; }
        /// <summary>
        /// 这个地得分归谁
        /// 0表示这个地上种子数量一样，都不得分
        /// 1表示这个地得分归玩家，玩家的数量多
        /// 2表示这个地得分归电脑，电脑数量多
        /// </summary>
        public int Winner { get {
            int r = MapUnit.OWNERNO;
            int u = 0, c = 0;//电脑和玩家的数量
            foreach (MapUnit i in units) {
                if (i.Owner == MapUnit.OWNERUSER) u++;
                else if (i.Owner == MapUnit.OWNERCOMPUTER) c++;
            }
            if (u > c) r = MapUnit.OWNERUSER;
            else if (c > u) r = MapUnit.OWNERCOMPUTER;
            return r;
        
        } }
        public int No { get; set; }//组编号
        /// <summary>
        /// 创建一个单元组
        /// </summary>
        /// <param name="no"></param>
        public Group(int no) {
            this.No = no;//当前的编号
            units = new List<MapUnit>();
        }

        public void Add(MapUnit unit) {
            unit.GroupNo = No;
            units.Add(unit);
        }
    }
}
